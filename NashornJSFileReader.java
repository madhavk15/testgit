package mydemo.pack;

import java.io.FileReader;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class NashornJSFileReader {

	public static void main(String[] args) throws Exception {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");

		String filePath = "D:\\Java\\work\\workspace\\OracleNashorn\\src\\mydemo\\pack\\";
		engine.eval(new FileReader(filePath + "math.js"));
		engine.eval(new FileReader(filePath + "operation.js"));

		Invocable invoke = (Invocable) engine;
		// callAdd
		//Object result = engine.eval("operation.add(10,20)");
		
		Object resultAdd = invoke.invokeFunction("callAdd", 10,20);
		System.out.println("Result " + resultAdd);
		
		Object resultSubtract = invoke.invokeFunction("callSubtract", 10,20);
		System.out.println("Result " + resultSubtract);
		
		Object resultMultiply = invoke.invokeFunction("callMultiply", 10,20);
		System.out.println("Result " + resultMultiply);
		
		Object resultDivide = invoke.invokeFunction("callDivide", 10,20);
		System.out.println("Result " + resultDivide);
		
	}
	
}
