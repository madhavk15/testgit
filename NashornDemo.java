package mydemo.pack;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class NashornDemo {

	public static void main(String[] args) throws ScriptException {
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("nashorn");
	
		
		engine.eval("var abc = function sum(a, b) { return a + b; }");
		System.out.println("Sum " + engine.eval("sum(1,2)"));
		/*
		engine.eval("function subtract(a, b) { return a - b; }");
		System.out.println("Subtraction " + engine.eval("subtract(10,5)"));
		
		engine.eval("function multiply(a, b) { return a * b; }");
		System.out.println("Multiply " + engine.eval("multiply(10,7)"));
		
		engine.eval("function divide(a, b) { return a / b; }");
		System.out.println("Divide " + engine.eval("divide(15,3)"));
		
		engine.eval("function mod(a, b) { return a % b; }");
		System.out.println("Mod " +engine.eval("mod(19,4)"));
		
		engine.eval("function concat(a, b) { return a + b; }");
		System.out.println("Concat " + engine.eval("concat('abc','xyz')"));
		
		/*
	    engine.eval("function consoleLog(a, b) { console.log( a + b ); }");
		System.out.println("Concat " + engine.eval("consoleLog('abc','xyz')"));
		*/
		
	}
}
