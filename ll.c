# include<stdio.h>
# include<stdlib.h>
# include<conio.h>


typedef struct node 
{
	struct node *prev;
	int data;
	struct node *next;
} node;

node * create();
node *insert_first(node *);
node *insert_last(node *);
node *insert_position(node *);
int count_nodes(node *);
node *delete_first(node *);
node *delete_last(node *);
node *delete_position(node *);

node *create()
{
	node *head, *p, *q;
	int n, i, x;
	head = NULL;

	printf("How many nodes you want to create \n");
	scanf("%d", &n);
	for (i = 1; i <= n; i++)
	{
		printf("Enter data \n");
		scanf("%d", &x);
		q = (node*)malloc(sizeof(node));
		q->data = x;
		q->prev = q->next = NULL;
		
		if (head == NULL)
		{
			head = q;
			head->next = head->prev = head;
		}
		else
		{
			p = head->prev;
			q->prev = p;
			p->next = q;
			q->next = head;
			head->prev = q;
		}
	}
	return head;
}

void print_forward(node *head)
{
	node *p, *q;
	p = head;
	do
	{
		printf("%d \t",p->data);
		p = p->next;
	} while (p != head);
	printf("\n");
}

void print_backward(node *head)
{
	node *p;
	p = head->prev;
	do
	{
		printf("%d \t", p->data);
		p = p->prev;
	} while (p != head->prev);
	printf("\n");
}

int main(void)
{
	node *head;
	int ch, ch2,count=0;
	head = create();
	while (1)
	{
		printf("\n 1.Print in Forward Direction.\n 2.Print in Reverse Direction.\n 3.Insert\n 4.Delete\n 5.Count Nodes of Linked List\n 6.Exit\n");
		scanf("%d", &ch);
		switch (ch)
		{
		case 1: printf("\nPrinting in Forward\n");
				print_forward(head);
				break;

		case 2: printf("\nPrinting in Backward\n");
				print_backward(head);
				break;

		case 3: printf("\n1. Insert at First position\n2. Insert at last poition\n3. Insert at given position\n4. Exit\n");
			printf("Enter your choice\n");
			scanf("%d", &ch2);
			switch (ch2)
			{
			case 1: printf("\nInserting at First location\n");
				head = insert_first(head);
				break;
			case 2: printf("\nInserting at First location\n");
				head = insert_last(head);
				break;
			case 3: printf("\nInserting at custom position \n");
				head = insert_position(head);
				break;
			case 4: break;

			default: printf("\nInvalid choice\n"); break;
			}
			break;

		case 4: printf("\n1. Delete at First position\n2. Delete at last poition\n3. Delete at position\n4. Exit\n");
			printf("Enter your choice\n");
			scanf("%d", &ch2);
			switch (ch2)
			{
			case 1: printf("\nDelete at First location\n");
				head = delete_first(head);
				break;
			case 2: printf("\nDelete at last location\n");
				head = delete_last(head);
				break;

			case 3:  printf("\nDelete at custom location\n");
				head = delete_position(head);
				break;

			case 4: break;

			default: printf("\nInvalid choice\n"); break;
			}
			break;

		case 5: count = count_nodes(head);
				printf("\nTotal number of nodes in linked list %d \n", count);
				break;
		
		case 6: exit(0); 
				break;

		default: printf("\nInvalid Choice\n");
				break;
		}

	}

	_getch();
	return 0;
}

int count_nodes(node *head)
{
	node *p;
	int n=0;
	p = head;
	do
	{
		p = p->next;
		++n;
	} while (p != head);
	return n;
}

node *insert_first(node *head)
{
	node *p, *q;
	int x;

	printf("Enter data to insert \n");
	scanf("%d", &x);
	q = (node *)malloc(sizeof(node));
	q->data = x;
	q->next = q->prev = NULL;

	if (head == NULL)
	{
		head = q;
		head->next = head->prev = head;
		return head;
	}
	else
	{
		p = head;
		q->next = p;
		p->prev->next = q;
		q->prev = p->prev;
		p->prev = q;
	}
	return head;
}

node *insert_last(node *head)
{
	node *p, *q;
	int x;

	printf("Enter data to insert \n");
	scanf("%d", &x);
	q = (node *)malloc(sizeof(node));
	q->data = x;
	q->next = q->prev = NULL;

	if (head == NULL)
	{
		head = q;
		head->next = head->prev = head;
		return head;
	}
	else
	{
		p = head->prev;
		p->next = q;
		q->prev = p;
		q->next = head;
		head->prev = q;
	}
	return head;
}

node *insert_position(node *head)
{
	node *p=NULL, *q=NULL;
	int count, pos, i=1;
	count = count_nodes(head);

	printf("Enter the position to insert\n");
	scanf("%d", &pos);
	if (pos == 1)
	{
		head = insert_first(head);
		return head;
	}
	else if (pos == count)
	{
		head = insert_last(head);
		return head;
	}
	else
	{
		p = head;
		while (i < pos)
		{
			p = p->next;
			++i;
		}
		q->next = p->next;
		p->next->prev = q;
		p->next = q;
		q->prev = p;
		
		return head;
	}
}

node *delete_first(node *head)
{
	node *p;
	if (head == NULL)
	{
		printf("\nNothing to delete \n");
		return NULL;
	}
	p = head->next;
	head->prev->next = p;
	p->prev = head->prev;
	free(head);
	return p;
}

node * delete_last(node *head)
{
	node *p, *q;
	int count;
	count = count_nodes(head);
	if (head == NULL)
	{
		printf("Nothing to delete\n");
		return NULL;
	}
	else if (count = 1)
	{
		free(head);
		return 0;
	}
	else
	{
		p = head;
		q = head->prev;

		q->prev->next = p;
		p->prev = q->prev;
		free(q);

		return p;
	}
	
}

node *delete_position(node *head)
{
	node *p, *q;
	int count, pos, i = 1;
	count = count_nodes(head);

	printf("Enter the position to delete\n");
	scanf("%d", &pos);
	if (pos == 1)
	{
		head = delete_first(head);
		return head;
	}
	else if (pos == count)
	{
		head = delete_last(head);
		return head;
	}
	else
	{
		p = head;
		while (i < pos)
		{
			p = p->next;
			++i;
		}
		q = p->next;
		p->next = q->next;
		q->next->prev = p;
		free(q);

		return head;
	}

}