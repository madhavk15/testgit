# include<stdio.h>
# include<conio.h>
# include<stdlib.h>

typedef struct node
{
	struct node *prev;
	int data;
	struct node *next;
}node;

node *create();
void print_forward(node *);
void print_reverse(node *);
node *insert_first();
node *insert_last();
node *insert_position(node *,int );
node *delete_first(node *);
node *delete_last(node *);
node *delete_position(node *,int );
int count(node *);

void main()
{
	node *head;
	int ch,ch1,ch2,pos,pos1,count1;
	head = NULL;
	head = create();
	while(1)
	{
		printf("\n 1.Print in Forward Direction.\n 2.Print in Reverse Direction.\n 3.Insert\n 4.Delete\n 5.Count Nodes of Linked List\n 6.Exit\n");
		printf("Enter your choice\n");
		scanf("%d",&ch);
		switch(ch)
		{
		case 1 : printf("\nElements in forward direction\n");
				 print_forward(head);
				 break;
		case 2 : printf("\nElements in reverse direction\n");
				 print_reverse(head);
				 break;

		case 3 : printf("\n1. Insert at First position\n2. Insert at last poition\n3. Insert at given position\n4. Exit\n");
				 printf("Enter your choice\n");
				 scanf("%d",&ch1);
				 switch(ch1)
				 {
				 case 1 : printf("\nInserting at First location\n");
						   head = insert_first(head);
						   break;
				 case 2 : printf("\nInserting at First location\n");
						   head = insert_last(head);
						   break;
				 case 3:  printf("Enter position of node to insert\n");
						  scanf("%d",&pos1);
						  head = insert_position(head,pos1);
						  break;

				 default : printf("\nInvalid choice\n"); break;
				 }
				 break;

		case 4 : printf("\n1. Delete at First position\n2. Delete at last poition\n3. Delete at position\n4. Exit\n");
				 printf("Enter your choice\n");
				 scanf("%d",&ch2);
				 switch(ch2)
				 {
				 case 1 : printf("\nDelete at First location\n");
						   head = delete_first(head);
						   break;
				 case 2 : printf("\nDelete at last location\n");
						   head = delete_last(head);
						   break;

				 case 3:  printf("Enter position of node to delete\n");
						  scanf("%d",&pos);
						  head = delete_position(head,pos);
						  break;
				
				 case 4 : break;

				 default : printf("\nInvalid choice\n"); break;
				 }
				 break;

		case 5: 
				count1 = count(head);
				printf("Total number of nodes in linked list %d \n",count1);
				break;

		case 6: exit(0);
		default : printf("\nInvalid choice\n");break;
		}
	}

	getch();
}

node *create()
{
	node *h,*p,*q;
	int n,i,x;

	h=NULL;

	printf("Enter number of elements... \n");
	scanf("%d",&n);

	for(i=0;i<n;i++)
	{
		printf("Enter data...\n");
		scanf("%d",&x);

		q = (node *)malloc(sizeof(node));
		q->data = x;
		q->next = q->prev =NULL;
		// If head is NULL i.e. List is empty
		if(h == NULL)
			p = h = q;
		else
		{
			p->next = q;
			q->prev = p;
			p = q;
		}
	}
		return (h);
}

void print_forward(node *h)
{
	while(h != NULL)
	{
		printf("%d\t",h->data);
		h = h->next;
	}
	printf("\n");
}

void print_reverse(node *h)
{
	while(h->next != NULL)
		h = h->next;
	while(h != NULL)
	{
		printf("%d\t",h->data);
		h = h->prev;
	}
	printf("\n");
}


node *insert_first(node *head)
{
	node *p,*q;
	int x,i;

	printf("Enter number to insert\n");
	scanf("%d",&x);

	q = (node *)malloc(sizeof(node));
	q->data = x;
	q->next = q->prev = NULL;

	if(head == NULL)
	{
		head = q;
		return head;
	}

	head->prev = q;
	q->next = head;
	
	head = q;

	return (head);
}

node *insert_last(node *head)
{
	node *p,*q;
	
	int x,i;
	p = head;

	printf("Enter number to insert at last position\n");
	scanf("%d",&x);

	q = (node *)malloc(sizeof(node));
	q->data = x;
	q->next = q->prev = NULL;

	if(head == NULL)
	{
		head = q;
		return head;
	}
	else
	{	
		//p = head;
		while(p->next != NULL)
			p = p->next;
		printf("\nLast node data %d \n",p->data);
		p->next = q;
		q->prev = p;
		q->next = NULL;
		
	}
	return (head);
}
node *insert_position(node *h,int pos)
{
	node *p,*q,*r;
	int n,x,i=1;
	n = count(h);
	p = h;
	
	if(pos == 1)
	{
		h = insert_first(h);
		return h;
	}

	else if(pos == n+1)
	{
		h = insert_last(h);
		return h;
	}
	else if(pos > n)
	{
		printf("Invalid position selected..\n");
		return h;
	}
	else 
	{
		// Allocate node for new data
		q = (node *)malloc(sizeof(node));
		printf("Enter number to insert..\n");
		scanf("%d",&(q->data));
		q->prev = q->next =NULL;

		while(i != pos-1)
		{
			p = p->next;
			++i;
		}
		
		p->next->prev = q;
		q->next = p->next;
		q->prev = p;
		p->next = q;
	}

	return (h);
}
node *delete_first(node *head)
{
	node *p;
	if(head == NULL)
	{
		printf("Nothing to delete...\n");
		return 0;
	}
	else
	{
		p = head;
		head = head ->next;
		head->prev = NULL;
		free(p);
	}
	return (head);
}

node *delete_last(node *head)
{
	node *p,*q;
	if(head == NULL)
	{
		printf("Nothing to delete...\n");
		return 0;
	}
	else if(head == NULL)
	{
		head = NULL;
		free(head);
		return NULL;
	}

	else
	{
		p = head;
		while(p->next->next != NULL)
			p = p->next;
		q = p->next;
		q->prev = NULL;
		p->next = NULL;
		free(q);
	}
	return (head);
}


node *delete_position(node *head,int pos)
{
	node *p,*q,*r;
	int n,x,i=1;
	r = p = head;
	// Count the number of nodes in Linked List
	n = count(r);
	// Check validity of number
	if(pos > n)
	{
		printf("Invalid position entered\n");
		return NULL;
	}

	else if(pos == 1)
		head = delete_first(r);

	else if(pos == n)
		head = delete_last(r);
		
	else 
	{
		while(i < pos)
		{
			p = p->next;
			++i;
		}

		q = p->prev;
		q->next = p->next;
		p->next->prev = q;
		free(p);
	}
	return head;
}

int count(node *r)
{
	int i=0;
	while(r != NULL)
	{
		++i;
		r = r->next;
	}
	return i;
}
